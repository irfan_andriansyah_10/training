import { Injectable } from '@angular/core'
import { Subject } from 'rxjs/Subject'
import { Observable } from 'rxjs/Rx'
import { todoInterface } from '../interface/todo.interface'

@Injectable()
export class TodoService{
    todos : todoInterface[] = [];

    constructor() {}

    addTodo(todo: todoInterface): any {
        this.todos.push(todo);
        return this;
    }

    getTodo() : todoInterface[] {
        return this.todos;
    }
}
