import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { WidgetsModule } from './widget.module';

import { TodoService } from '../service/todo.service'

import { TodoComponent }  from '../component/todo/todo.component';

@NgModule({
  imports: [BrowserModule, WidgetsModule, FormsModule],
  declarations: [TodoComponent],
  providers: [TodoService],
  bootstrap: [TodoComponent]
})
export class AppModule { }
