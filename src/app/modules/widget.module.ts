import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InputWidgetComponent } from '../widgets/input/input.component'
import { ListWidgetComponent } from '../widgets/list/list.component'
import { ItemWidgetComponent } from '../widgets/list-item/item.component'

@NgModule({
    imports : [CommonModule, ReactiveFormsModule],
    declarations : [InputWidgetComponent, ListWidgetComponent, ItemWidgetComponent],
    exports : [InputWidgetComponent, ListWidgetComponent, ItemWidgetComponent]
})
export class WidgetsModule {}
