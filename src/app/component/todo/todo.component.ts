import { Component } from '@angular/core';
import { TodoService } from '../../service/todo.service'

@Component({
  selector: 'my-app',
  templateUrl: './todo.component.html'
})
export class TodoComponent  {
    todo : any = []

    constructor(private todoService : TodoService) {}

    onAddTodo(todo:any):void {
        this.todoService.addTodo(todo)
    }

    get todos():any {
        return  this.todoService.getTodo()
    }

}
