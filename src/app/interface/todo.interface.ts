export interface todoInterface {
    message : string,
    time : string,
    timestamp : number,
    active : boolean
}
