 import { Component, Output, EventEmitter } from '@angular/core';
import { todoInterface } from '../../interface/todo.interface'
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'search-widget',
    templateUrl: './input.component.html'
})
export class InputWidgetComponent {
    @Output() add : EventEmitter<any> = new EventEmitter();

    data : todoInterface;
    form : FormGroup;
    input : any;

    constructor(private fb: FormBuilder) {}

    ngOnInit() {
        let date = new Date(Date.now())
        this.form = this.fb.group({
            message : ['', [<any>Validators.required, <any>Validators.minLength(4)]]
        })
    }

    getMonth(month : number) {
        var monthNames = ['01','02','03','04','05','06','07','08','09','10','11','12']
        return monthNames[month]
    }

    onSubmit(form: todoInterface, valid: boolean): void {
        let time = new Date()
        form['time'] = `${time.getFullYear()}/${this.getMonth(time.getMonth())}/${time.getDate()} ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`

        form['timestamp'] = Date.now()
        form['active'] = true

        valid ? this.form.reset() : '';
        valid ? this.add.emit(form) : '';
    }
}
