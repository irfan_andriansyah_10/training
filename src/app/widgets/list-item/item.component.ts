import { Component, EventEmitter, Input } from '@angular/core'
import { todoInterface } from '../../interface/todo.interface'

@Component({
    selector : 'item-widget',
    templateUrl : './item.component.html'
})

export class ItemWidgetComponent {
    @Input() todos : todoInterface;

    constructor() {
        console.log(this.todos)
    }
}
