import { Component, EventEmitter, Input} from '@angular/core'
import { todoInterface } from '../../interface/todo.interface'

@Component({
    selector : 'list-widget',
    templateUrl : './list.component.html'
})

export class ListWidgetComponent {
    @Input() todos : todoInterface[];

    constructor() {}
}
